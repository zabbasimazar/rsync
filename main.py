import sys
import os
import shutil
import filecmp
import time
import gamin


__author__ = "Zeinab Abbasimazar"


def copy_files(source_dir, destination_dir):
    for item in filter(lambda x: os.path.isfile(os.path.join(source_dir, x)), os.listdir(source_dir)):
        if not os.path.exists(os.path.join(destination_dir, item)):
            sys.stdout.write('destination does not contain file "{0}". going to copy...\n'.format(item))
            shutil.copy(os.path.join(source_dir, item), destination_dir)
        elif not filecmp.cmp(os.path.join(source_dir, item), os.path.join(destination_dir, item)):
            sys.stdout.write('destination and source are not the same ("{0}"). going to copy...\n'.format(item))
            os.remove(os.path.join(destination_dir, item))
            shutil.copy(os.path.join(source_dir, item), destination_dir)


def copy_all(src_path, dst_path):
    copy_files(source_dir=src_path, destination_dir=dst_path)
    directories = [d for d in os.listdir(src_path) if os.path.isdir(os.path.join(src_path, d))]
    for d in directories:
        if not os.path.exists(os.path.join(dst_path, d)):
            os.mkdir(os.path.join(dst_path, d))
        copy_all(src_path=os.path.join(src_path, d), dst_path=os.path.join(dst_path, d))


if __name__ == "__main__":

    while True:
        source_dir = sys.argv[1]
        destination_dir = sys.argv[2]
        if not os.path.exists(source_dir) or not os.path.exists(destination_dir):
            sys.stdout.write('no such directory\n')
            sys.exit(1)
        if not os.path.isdir(source_dir) or not os.path.isdir(destination_dir):
            sys.stdout.write('no such directory\n')
            sys.exit(1)

        copy_all(src_path=source_dir, dst_path=destination_dir)

        sys.stdout.write('Done.\n')

        time.sleep(1)